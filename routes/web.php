<?php

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SessionController;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminPostController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\PostCommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Admin route
 *
 */
Route::group([
    'controller' => AdminPostController::class,
    'middleware' => ['auth', 'isAdmin'],
    'as' => 'posts-',
    'prefix' => 'admin/posts'
], function () {
    Route::get('/', 'index')->name('index');
    Route::get('/create', 'create')->name('create');
    Route::post('/save', 'store')->name('store');
    Route::get('/{post:slug}/edit', 'edit')->name('edit');
    Route::put('/{post:slug}/update', 'update')->name('update');
    Route::delete('/{post}/delete', 'destroy')->name('delete');

});

/**
 * Post public view
 *
 */
Route::group([
    'controller' => PostController::class,
], function () {
    Route::get('/', 'index')->name('home');
    Route::get('/posts/{post:slug}', 'show')->where('post', '[A-z_\-]+')->name('show-post');
});

/**
 * Register route
 *
 */
Route::group([
    'controller' => RegisterController::class,
    'middleware' => 'guest'
], function () {
    Route::get('/register', 'create')->name('register');
    Route::post('/register', 'store');
});

/**
 * Login route
 *
 */
Route::group([
    'controller' => SessionController::class,
], function () {
    Route::post('/logout', 'destroy')->middleware('auth:web')->name('logout');
    Route::get('/login', 'create')->middleware('guest')->name('login');
    Route::post('/login', 'store')->middleware('guest');
});

/**
 * Comment for logged in user
 *
 */
Route::group([
    'controller' => PostCommentController::class,
    'middleware' => ['auth'],
    'as' => 'comments-'
], function () {
    Route::post('/posts/{post:slug}/comment', 'store')->name('store');
});

/**
 * Subscribe newsletter
 *
 */
Route::post('/newsletter', NewsletterController::class)->name('newsletter-store');
