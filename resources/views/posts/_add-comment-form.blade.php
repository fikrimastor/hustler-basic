@auth
<x-panel>
    <form action="{{ route('comments-store', ['post' => $post->slug]) }}" method="POST">
        @csrf
        <header class="flex items-center">
            <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" width="40" height="40" alt="{{ auth()->id() }}"
                class="rounded-full">
            <h2 class="ml-4">
                Want to participate?
            </h2>
        </header>

        <div class="mt-6">
            <textarea name="body" class="w-full text-sm focus:outline-none focus:ring"
                rows="5" placeholder="Quick, share some ideas!" required>
            </textarea>
            <x-form.error name="body"/>
        </div>

        <div class="flex justify-end pt-6 mt-6 border-t border-gray-200">
            <x-form.button>Post</x-form.button>
        </div>
    </form>
</x-panel>
@else
<p class="font-semibold">
    <a href="{{ route('register') }}" class="hover:underline">
        Register
    </a>
    or
    <a href="{{ route('login') }}" class="hover:underline">
        Log In
    </a>
    to leave a comment.
</p>
@endauth
