<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-lg p-8 mx-auto mt-10 bg-gray-100 border border-gray-200 rounded-xl">
            <h1 class="text-xl font-bold text-center">
                Register!
            </h1>
            <form action="{{ route('register') }}" method="post" class="mt-6">
                @csrf
                <x-form.input name="name" type="text" />
                <x-form.input name="username" type="text" />
                <x-form.input name="email" type="email" autocomplete="username" />
                <x-form.input name="password" type="password" autocomplete="new-password" />
                <x-form.button>Login</x-form.button>
            </form>
        </main>
    </section>
</x-layout>
