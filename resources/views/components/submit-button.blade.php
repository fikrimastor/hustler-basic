<button type="submit" {{$attributes(['class' => 'px-10 py-2 font-semibold text-white uppercase bg-blue-500 hover:bg-blue-600 rounded-2xl'])}}>
    {{$slot}}
</button>
