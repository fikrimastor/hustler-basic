@props(['name'])
<x-form.field>
    <x-form.label name="{{$name}}"/>

    <textarea
        class="w-full p-2 border border-gray-200 rounded"
        name="{{$name}}"
        id="{{$name}}"
        rows="5"
        placeholder="{{$name}}"
        required
        {{$attributes}}>

        {{$slot ?? old($name)}}

    </textarea>

    <x-form.error name="{{$name}}"/>
</x-form.field>
