<x-form.field>
    <button type="submit" class="px-4 py-2 text-white bg-blue-500 rounded hover:bg-blue-600">
        {{$slot}}
    </button>
</x-form.field>
