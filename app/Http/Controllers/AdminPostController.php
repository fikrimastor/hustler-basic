<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PostService;
use App\Http\Requests\StorePostRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdatePostRequest;

class AdminPostController extends Controller
{
    public function __construct(
        protected PostService $postService
    )
    {
        //
    }

    public function index()
    {
        return view('admin.posts.index', [
            'posts' => $this->postService->posts()
        ]);
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(StorePostRequest $request)
    {
        $validated = $request->validated();

        $validated['user_id'] = auth()->id();

        $validated['thumbnail'] = $this->postService->uploadImage($request->file('thumbnail'));

        $post = $this->postService->create($validated);

        return redirect()->route('posts-index')->with('success', 'You have create new post!');
    }

    public function edit($post)
    {
        $object = $this->postService->post($post);

        return view('admin.posts.edit', [
            'post' => $object
        ]);
    }

    public function update(UpdatePostRequest $request, string $post)
    {
        $validated = $request->validated();

        if ($request->file('thumbnail')) {
            $validated['thumbnail'] = $this->postService->uploadImage($request->file('thumbnail'));
        }

        $object = $this->postService->update($validated, $post);

        return redirect()->route('posts-index')->with('success', 'You have edit the post!');
    }

    public function destroy($post)
    {
        $this->postService->delete($post);

        return redirect()->route('posts-index')->with('success', 'You have delete the post!');
    }
}
