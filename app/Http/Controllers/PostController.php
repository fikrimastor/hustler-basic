<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\PostService;
use App\Http\Requests\StorePostRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostController extends Controller
{
    public function __construct(
        protected PostService $postService
    )
    {
        //
    }

    public function index()
    {
        $array = request()->only('search', 'category', 'author');

        return view('posts.index', [
            'posts' => Post::latest()->filter($array)->paginate(24)->withQueryString()
        ]);
    }

    public function show($post)
    {
        $object = $this->postService->postBySlug($post);

        return view('posts.show', [
            'post' => $object
        ]);
    }
}
