<?php

namespace App\Http\Controllers;

use Exception;
use App\Services\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class NewsletterController extends Controller
{
    public function __construct(
        protected Newsletter $newsletter
    )
    {
        //
    }

    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required', 'email']
        ]);

        try {
            $response = $this->newsletter->subscribe($validated['email']);
        } catch (Exception $e) {
            throw ValidationException::withMessages([
                'email' => 'This email could not be added to our newsletter list.'
            ]);
        }

        return back()->with('success', 'You are now signed up for our newsletter');
    }
}
