<?php

namespace App\Services;

use App\Models\Post;
use Illuminate\Support\Facades\Storage;

/**
 * Class PostService.
 */
class PostService
{
    public function post(string $post)
    {
        return Post::whereId($post)->firstOrFail();
    }

    public function postBySlug(string $post)
    {
        return Post::whereSlug($post)->firstOrFail();
    }

    public function posts()
    {
        return Post::paginate(50);
    }

    public function create(array $postData)
    {
        return Post::create($postData);
    }

    public function update(array $postData, string $post)
    {
        $object = $this->post($post);

        return $object->update($postData);
    }

    public function delete(string $post)
    {
        $object = $this->post($post);

        return $object->delete();
    }

    public function uploadImage($thumbnail)
    {
        return Storage::disk('public')
            ->putFileAs(
                'thumbnails',
                $thumbnail,
                str_replace(' ', '', microtime()) . '.' . $thumbnail->extension()
            );
    }

    // public function generateCode($lenght = 13) {
    //     // uniqid gives 13 chars, but you could adjust it to your needs.
    //     if (function_exists("random_bytes")) {
    //         $bytes = random_bytes(ceil($lenght / 2));
    //     } elseif (function_exists("openssl_random_pseudo_bytes")) {
    //         $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    //     } else {
    //         throw new Exception("no cryptographically secure random function available");
    //     }
    //     return substr(bin2hex($bytes), 0, $lenght);
    // }
}
