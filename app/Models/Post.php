<?php

namespace App\Models;

use App\Models\User;
use App\Models\Comment;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    // protected $fillable = [
    //     'title',
    //     'slug',
    //     'excerpt',
    //     'body',
    //     'published_at',
    //     'category_id',
    // ];

    // protected $guarded = [
    //     'id'
    // ];

    protected $with = ['category', 'author'];

    /**
     * Optional if want use model binding but with different key, in this case slug instead of id.
     */
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }
    public function scopeFilter($query, array $array) // Post::someQuery()->filter()
    {
        // if($array['search'] ?? false) {
        //     $query
        //         ->where('title', 'like', '%' . request('search') . '%')
        //         ->orWhere('body', 'like', '%' . request('search') . '%');
        // }

        $query->when($array['search'] ?? false, fn ($query, $search) =>
            $query->where(fn($query) =>
                $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('body', 'like', '%' . $search . '%')
            )
        );

        $query->when($array['category'] ?? false, fn ($query, $category) =>
            $query
                ->whereHas('category', fn($query) =>
                    $query->where('slug', $category)
                )
        );

        $query->when($array['author'] ?? false, fn ($query, $author) =>
            $query
                ->whereHas('author', fn($query) =>
                    $query->where('username', $author)
                )
        );

    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
