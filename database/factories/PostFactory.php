<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = fake()->name();
        $slug = Str::slug($name,'-');

        return [
            'title' => $name,
            'slug' => $slug,
            // 'excerpt' => '<p>' . fake()->words(13,true) . '</p>',
            // 'body' => '<p>' . fake()->paragraph() . '</p>',
            'excerpt' => '<p>' . implode('</p><p>', fake()->paragraphs(2)) . '</p>',
            'body' => '<p>' . implode('</p><p>', fake()->paragraphs(6)) . '</p>',
            'published_at' => now(),
            'category_id' => fake()->randomDigitNotZero(),
            'user_id' => User::factory(),
        ];
    }
}
